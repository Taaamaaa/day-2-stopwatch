let seconds = 0;
let minutes = 0;
let hours = 0;
let timeRef = document.querySelector(".timer-display");
let int = null;

document.getElementById("start").addEventListener("click", () => {
    if (int !== null) {
        clearInterval(int);
    }
    int = setInterval(displayTimer, 1000);
});

document.getElementById("stop").addEventListener("click", () => {
    clearInterval(int);
});

document.getElementById("reset").addEventListener("click", () => {
    clearInterval(int);
    seconds = 0;
    minutes = 0;
    hours = 0;
    timeRef.innerHTML = `<div style="display: flex; justify-content: center; align-items: center;flex-direction: column">
  <p>00 : 00 : 00</p>
  <p>Jam : Menit : Detik</p>
</div>`;
});

function displayTimer() {
    seconds++;
    if (seconds === 60) {
        seconds = 0;
        minutes++;
        if (minutes === 60) {
            minutes = 0;
            hours++;
        }
    }

    let h = hours < 10 ? "0" + hours : hours;
    let m = minutes < 10 ? "0" + minutes : minutes;
    let s = seconds < 10 ? "0" + seconds : seconds;

    timeRef.innerHTML = `<div style="display: flex; justify-content: center; align-items: center;flex-direction: column">
  <p>${h} : ${m} : ${s}</p>
  <p>Jam : Menit : Detik</p>
</div>`;
}